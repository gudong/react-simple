
var express = require('express'); //加载express框架
var app = express();  //应用对象
var port = 8002;

var path = require('path');
app.use(express.static(path.join(__dirname, './')));
app.set('view engine', 'html'); //替换文件扩展名ejs为html

app.get('/', function (req, res) {
    res.sendFile(__dirname +'index.html');
});
app.get('/extern', function (req, res) {
    res.sendFile(__dirname +'/extern/index.html');
});
app.get('/bootstrap', function (req, res) {
    res.sendFile(__dirname +'/bootstrap/index.html');
});

var server = app.listen(port, function() {
    console.log('Express is listening to http://luobocode.cn:'+port);
});

